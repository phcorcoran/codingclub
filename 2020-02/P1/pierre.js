// https://www.codewars.com/kata/525d50d2037b7acd6e000534/train/javascript

Array.prototype.square = function() {
  a = [];
  this.forEach(e => a.push(e*e));
  return a;
};
Array.prototype.cube = function() {
  a = [];
  this.forEach(e => a.push(e*e*e));
  return a;
};
Array.prototype.sum = function() {
  a = 0;
  this.forEach(e => a += e);
  return a;
};
Array.prototype.average = function() {
  if(this.length < 1) return NaN;
  return this.sum()/this.length;
};
Array.prototype.even = function() {
  a = [];
  this.forEach(function(e) {
    if(e % 2 == 0) a.push(e);
  });
  return a;
};
Array.prototype.odd = function() {
  a = [];
  this.forEach(function(e) {
    if(e % 2 != 0) a.push(e);
  });
  return a;
};
