Array.prototype.square = function () {
    return this.map(num => num * num)
 }
 
 Array.prototype.cube = function () {
    return  this.map(num => num * num * num)
 }
 
 Array.prototype.sum = function () {
   return this.reduce((a,b) => a + b)
 }
 
 Array.prototype.average = function () {
   let sum = this.reduce((a,b) => a + b, 0) 
   return sum / this.length
 }
 
 Array.prototype.even = function () {
   return this.filter( num => num % 2 === 0)
 }
 
 Array.prototype.odd = function () {
   return this.filter( num => num % 2)
 } 