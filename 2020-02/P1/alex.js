Array.prototype.square = function() {
  let newArray = [];
  this.forEach((num, index) => {
    return newArray.push(Math.pow(num, 2));
  });
  // new copy
  const result = [...newArray];
  return result;
};

Array.prototype.cube = function() {
  let newArray = [];
  this.forEach((num, index) => {
    return newArray.push(Math.pow(num, 3));
  });
  // new copy
  const result = [...newArray];
  return result;
};

Array.prototype.average = function() {
  if (!this.length) {
    return NaN;
  } else {
    let total = this.reduce((acc, num) => {
      return acc + num;
    });

    return total / this.length;
  }
};

Array.prototype.sum = function() {
  return this.reduce((acc, num) => {
    return num + acc;
  });
};

Array.prototype.even = function() {
  let newArray = [];
  this.filter(num => {
    if (num % 2 === 0) {
      newArray.push(num);
    }
  });

  return newArray;
};

Array.prototype.odd = function() {
  let newArray = [];
  this.filter(num => {
    if (num % 2 !== 0) {
      newArray.push(num);
    }
  });

  return newArray;
};
