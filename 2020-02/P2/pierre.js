function incrementString (s) {
  var i = s.length - 1;
  while('0123456789'.includes(s[i])) i--;
  a = s.slice(i+1);
  x = 1;
  if(a.length > 0) {
    x = String(parseInt(a) + 1);
    while(a.length > x.length) x = '0'+x;
  }
  return s.slice(0, i+1) + x;
}
