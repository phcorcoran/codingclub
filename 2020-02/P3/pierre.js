function kebabize(s) {
  return s[0].toLowerCase().replace(/[^a-z]/g, '') + s.slice(1).replace(/([A-Z])/g, '-$1').toLowerCase().replace(/[^a-z-]/g, '')
}
