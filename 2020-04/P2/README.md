Using the input file from the previous challenge, make a script such that:

```
node myawesomescript.js codingclub.csv
```

will create a file named 'codingclub_2020-04.html' and open it in a new web browser tab.

The content of the file should be a series of rectangles each on their own line, with the RGB color being set from the first 3 values of the file row, shifted by 100 (first number+100 red, second number+100 green, third number+100 blue) and the height and width from the last 2 values.

The HTML file should contain valid HTML formatting, with a header and body (https://validator.w3.org/#validate_by_upload)

Result should look like this: https://bitbucket.org/phcorcoran/codingclub/src/master/2020-04/P2/result.png