const fs = require('fs');
// Filesystem module (internal node module)
const inuputFileName = process.argv[2];
const outputFileName = process.argv[3];

let rows = fs.readFileSync(`${inuputFileName}`, 'utf8').split('\n');
// reads the data and breaks them into rows

function sum(accumulator, a) {
    return accumulator + a;
}

function mean(arr) {
    return arr.reduce(sum, 0) / arr.length;
}

function standardDeviation(arr) {
    let sum = 0;
    let avg = mean(arr);
    for (let i = 0; i < arr.length; i++) {
        sum += Math.pow(arr[i] - avg, 2);
    }
    return Math.round((Math.sqrt(sum / (arr.length - 1))) * 100 + Number.EPSILON) / 100;
}
// all 3 functions required
const resultArr = [];
for (let r = 0; r < rows.length; r++) {
    resultArr.push([rows[r].split(',').map(Number).reduce(sum, 0),
        mean(rows[r].split(',').map(Number)),
    standardDeviation(rows[r].split(',').map(Number))])
} 
// logic for getting all 3 operations placed as a triple sub array
let output = fs.createWriteStream(`${outputFileName}`);
output.on('error', err => console.log(err));
resultArr.forEach(row => output.write(row.join(',') + '\n'));
output.end();
// writing back to a file w/ some manipulation
