Make sure of the following before running my script:

1 - Must be in the directory (jt_solution)
2 - Have the sample data file in the same directory
3 - Values must be a number and they must be separated by commas

I already included the sample data 'coding.csv' file in the directory.

How to run script:
node script.js sample_data_filename.csv output_filename.csv

Result:
The output_filename.csv will be created in the directory. Open it up and see the results